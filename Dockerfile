FROM python:3-alpine

ENV user ""
ENV password ""
ENV email ""

WORKDIR /app

RUN mkdir /app/db
COPY . .
RUN pip install --no-cache-dir -r requirements.txt


EXPOSE 8000
VOLUME ["/app/db"]

CMD sh init.sh && python manage.py runserver 0.0.0.0:8000
